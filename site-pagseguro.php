<?php
# *****
# File: site-pagseguro.php
# Project: Curso de PHP com PagSeguro da Hcode
# Path: ~desenvolvedor/curso-de-php-com-pagseguro-hcode
# File Created: Monday, 17 May 2021 01:39:43
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Saturday, 09 July 2022 11:51:17
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2021 - 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Principais Rotas do PagSeguro.
# *****

use Hcode\Page;
use Hcode\Model\User;
use Hcode\PagSeguro\Config;
use Hcode\PagSeguro\Transporter;
use Hcode\PagSeguro\Document;
use Hcode\Model\Order;
use Hcode\PagSeguro\Phone;
use Hcode\PagSeguro\Address;

$app->post('/payment/credit', function () {
	User::verifyLogin(false);
	$order = new Order();
	$order->getFromSession();
	$order->get((int)$order->getidorder());
	$address = $order->getAddress(); // Obtém o endereço do usuário para validação de segurança da operadora de cartão de crédito.
	$cart = $order->getCart(); // Obtém o carrinho de compras.
	$cpf = new Document(Document::CPF, $_POST['cpf']);
	$phone = new Phone($_POST['ddd'], $_POST['phone']);

	$shippingAddress = new Address(
		$address->getdesaddress(),
		$address->getdesnumber(),
		$address->getdescomplement(),
		$address->getdesdistrict(),
		$address->getdeszipcode(),
		$address->getdescity(),
		$address->getdesstate(),
		$address->getdescountry()
	);

	var_dump($order);
	var_dump($address);
	var_dump($shippingAddress);

	$dom = new DOMDocument();
	$test = $shippingAddress->getDOMElement();
	$testNode = $dom->importNode($test, true);
	$dom->appendChild($testNode);
	echo $dom->saveXML(); //UNDONE: Parei aqui em 15m42s de 52m24s.
});

$app->get('/payment', function () {
	User::verifyLogin(false);
	$order = new Order();
	$order->getFromSession();
	$years = [];
	for ($y = date('Y'); $y < date('Y') + 14; $y++) {
		array_push($years, $y);
	}
	$page = new Page([
		'footer' => true // para reativar o jquery é necessário o footer template disponível.
	]);
	$page->setTpl('payment', [
		'order' => $order->getValues(),
		'msgError' => Order::getError(),
		'years' => $years,
		'pagseguro' => [
			'urlJS' => Config::getUrlJS(),
			'id' => Transporter::createSession(),
			'maxInstallmentNoInterest' => Config::MAX_INSTALLMENT_NO_INTEREST,
			'maxInstallment' => Config::MAX_INSTALLMENT
		]
	]);
});
