<?php
# *****
# File: site-cart.php
# Project: Curso de PHP com PagSeguro da Hcode
# Path: ~desenvolvedor/curso-de-php-com-pagseguro-hcode
# File Created: Tuesday, 19 January 2021 12:34:24
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Monday, 11 July 2022 12:10:20
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2021 - 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Slim Cart
# *****

use Hcode\Page;
use Hcode\Model\Cart;
use Hcode\Model\Product;

$app->get("/cart", function () {

	$cart = Cart::getFromSession();

	$page = new Page();

	$page->setTpl("cart", [
		'cart' => $cart->getValues(),
		'products' => $cart->getProducts(),
		'error' => Cart::getMsgError()
	]);
});

$app->get("/cart/:idproduct/add", function ($idproduct) {

	$product = new Product();

	$product->get((int)$idproduct);

	$cart = Cart::getFromSession();

	$qtd = (isset($_GET['qtd'])) ? (int)$_GET['qtd'] : 1;

	for ($i = 0; $i < $qtd; $i++) {

		$cart->addProduct($product);
	}

	header("Location: /cart");
	exit;
});

$app->get("/cart/:idproduct/minus", function ($idproduct) {

	$product = new Product();

	$product->get((int)$idproduct);

	$cart = Cart::getFromSession();

	$cart->removeProduct($product);

	header("Location: /cart");
	exit;
});

$app->get("/cart/:idproduct/remove", function ($idproduct) {

	$product = new Product();

	$product->get((int)$idproduct);

	$cart = Cart::getFromSession();

	$cart->removeProduct($product, true);

	header("Location: /cart");
	exit;
});

$app->post("/cart/freight", function () {

	$cart = Cart::getFromSession();

	$cart->setFreight($_POST['zipcode']);

	header("Location: /cart");
	exit;
});
