# anotações

Última atualização: Sunday, 03 July 2022 16:39:27

## sobre a execução do projeto

- é óbvio, mas levantar os serviços do Apache e do mysql(MariaDB);
- verificar os detalhes de infraestrutura antes de iniciar;
- permissão dos diretórios são corrigidas pelo arquivo ~scripts/arrumar.permissoes.apache.pagseguro.sh;
- para efetuar a cópia de segurança do banco de dados de validação utilizar o script: ~/scripts/backup.mysql.pagseguro.sh

## para testar o produto

- Login e senha: admin / admin.
- Para administrar os usuários do site: <http://www.hcodepagseguro.com.br/admin>

## Erros e Soluções

### Handlebars não está executando dentro do curso de php7 com pagseguro

![atualizado_npm](20220704111723.png)
Atualizamos a versão do npm 8.5.0 para 8.13.2 como recomendado e não resolveu. Então trocamos a versão "runtime" para a versão "minificada" e voltou a funcionar.

![atualizado_handlebars](20220704120939.png)
voltou a funcionar tudo como dantes no quartel de Abrantes (rs).

## Problema durante a aula 28. Classe Payment na Seção 5: Classes do PagSeguro (10m20s) do Curso de PHP com PagSeguro Hcode

- [x] O problema, ao que tudo indica, é relativo a validação do CPF dentro da classe "Document". (Saturday, 09 July 2022 12:39:43)
  Resolvido: o parâmetro da função "isValidCPF" estava como "int" e não como "string". Veja o cabeçalho corrigido abaixo:

```php
  public static function isValidCPF(string $number): bool { (...)
```
