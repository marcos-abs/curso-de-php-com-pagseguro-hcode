<?php
# *****
# File: functions.php
# Project: curso-de-php-com-pagseguro-hcode
# File Created: Tuesday, 19 January 2021 12:34:24
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Wednesday, 27 October 2021 22:29:49
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) <<projectCreationYear>> - 2021 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# 				Functions Library
# *****

//cSpell:ignore inadmin, nrqtd

use \Hcode\Model\User;
use \Hcode\Model\Cart;

function formatPrice($vlprice) {

	if (!$vlprice > 0) $vlprice = 0;

	return number_format($vlprice, 2, ",", ".");
}

function formatDate($date) {

	return date('d/m/Y', strtotime($date));
}

function checkLogin($inadmin = true) {

	return User::checkLogin($inadmin);
}

function getUserName() {

	$user = User::getFromSession();

	return $user->getdesperson();
}

function getCartNrQtd() {

	$cart = Cart::getFromSession();

	$totals = $cart->getProductsTotals();

	return $totals['nrqtd'];
}

function getCartVlSubTotal() {

	$cart = Cart::getFromSession();

	$totals = $cart->getProductsTotals();

	return formatPrice($totals['vlprice']);
}
