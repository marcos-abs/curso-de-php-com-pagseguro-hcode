<?php
 # *****
 # File: index.php
 # Project: curso-de-php-com-pagseguro-hcode
 # File Created: Tuesday, 19 January 2021 12:34:24
 # Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 # -----
 # Last Modified: Monday, 17 May 2021 01:43:30
 # Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 # -----
 # Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 # -----
 # Description:
 #              Start do E-commerce
 # *****

session_start();

require_once("vendor/autoload.php");

use \Slim\Slim;

$app = new Slim();

$app->config('debug', true);

require_once("functions.php");
require_once("site.php");
require_once("site-cart.php");
require_once("site-checkout.php");
require_once("site-login.php");
require_once("site-profile.php");
require_once("site-pagseguro.php");
require_once("admin.php");
require_once("admin-users.php");
require_once("admin-categories.php");
require_once("admin-products.php");
require_once("admin-orders.php");

$app->run();
?>