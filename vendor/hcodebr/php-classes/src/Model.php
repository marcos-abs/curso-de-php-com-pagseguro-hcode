<?php
# *****
# File: Model.php
# Project: Curso de PHP com PagSeguro da Hcode
# Path: ~desenvolvedor/curso-de-php-com-pagseguro-hcode
# File Created: Tuesday, 19 January 2021 12:34:24
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Tuesday, 12 July 2022 13:41:58
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2021 - 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Classe Model.
# *****

namespace Hcode;

class Model {

	private $values = [];

	public function __call($name, $args) {

		$method = substr($name, 0, 3);
		$fieldName = substr($name, 3, strlen($name));

		switch ($method) {

			case "get":
				return (isset($this->values[$fieldName])) ? $this->values[$fieldName] : NULL;
				break;

			case "set":
				$this->values[$fieldName] = $args[0];
				break;
		}
	}

	public function setData($data = array()) {

		foreach ($data as $key => $value) {

			$this->{"set" . $key}($value);
		}
	}

	public function getValues() {

		return $this->values;
	}
}
