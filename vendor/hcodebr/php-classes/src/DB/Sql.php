<?php
# *****
# File: Sql.php
# Project: DB
# File Created: Tuesday, 19 January 2021 12:34:24
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Saturday, 02 July 2022 14:11:36
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
# -----
# Description:
# 				Configuração dos acessos ao banco de dados MySQL
# *****

namespace Hcode\DB;

class Sql {

	const HOSTNAME = "localhost";
	const USERNAME = "ecommerce";
	const PASSWORD = 'Fn2jpSs3H!Y&^Wqm^q8xuvc$yqP3EM';
	const DBNAME = "ecommerce";

	private $conn;

	public function __construct() {

		$this->conn = new \PDO(
			"mysql:dbname=" . Sql::DBNAME . ";host=" . Sql::HOSTNAME,
			Sql::USERNAME,
			Sql::PASSWORD
		);
	}

	private function setParams($statement, $parameters = array()) {

		foreach ($parameters as $key => $value) {

			$this->bindParam($statement, $key, $value);
		}
	}

	private function bindParam($statement, $key, $value) {

		$statement->bindParam($key, $value);
	}

	public function query($rawQuery, $params = array()) {

		$stmt = $this->conn->prepare($rawQuery);

		$this->setParams($stmt, $params);

		$stmt->execute();
	}

	public function select($rawQuery, $params = array()): array {

		$stmt = $this->conn->prepare($rawQuery);

		$this->setParams($stmt, $params);

		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}
}
