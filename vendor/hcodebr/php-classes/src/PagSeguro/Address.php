<?php
# *****
# File: Address.php
# Project: Curso de PHP com PagSeguro da Hcode
# Path: ~desenvolvedor/curso-de-php-com-pagseguro-hcode
# File Created: Wednesday, 06 July 2022 12:29:09
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Tuesday, 12 July 2022 13:43:23
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Classe Address.
# *****

namespace Hcode\PagSeguro;

use DOMElement;
use DOMDocument;

class Address {
    private $street;
    private $number;
    private $complement;
    private $district;
    private $postalCode;
    private $city;
    private $state;
    private $country;

    public function __construct(
        string $street,
        string $number,
        string $complement,
        string $district,
        string $postalCode,
        string $city,
        string $state,
        string $country
    ) {
        if (!$street) {
            throw new \Exception("Informe o logradouro do endereço.");
        }
        if (!$number) {
            throw new \Exception("Informe o número do endereço.");
        }
        if (!$district) {
            throw new \Exception("Informe o bairro do endereço.");
        }
        if (!$postalCode) {
            throw new \Exception("Informe o CEP do endereço.");
        }
        if (!$city) {
            throw new \Exception("Informe a município do endereço.");
        }
        if (!$state) {
            throw new \Exception("Informe o estado do endereço.");
        }
        if (!$country) {
            throw new \Exception("Informe o país do endereço.");
        }
        $this->street = $street;
        $this->number = $number;
        $this->complement = $complement;
        $this->district = $district;
        $this->postalCode = $postalCode;
        $this->city = $city;
        $this->state = $state;
        $this->country = $country;
    }
    public function getDOMElement($node = "address"): DOMElement { // o XML precisa do nó "address" mas o PagSeguro com a Operadora do cartão precisam do "BillingAddress"
        $dom = new DOMDocument();

        $address = $dom->createElement($node);
        $address = $dom->appendChild($address);

        $street = $dom->createElement("street", $this->street);
        $street = $address->appendChild($street);

        $number = $dom->createElement("number", $this->number);
        $number = $address->appendChild($number);

        $complement = $dom->createElement("complement", $this->complement);
        $complement = $address->appendChild($complement);

        $district = $dom->createElement("district", $this->district);
        $district = $address->appendChild($district);

        $city = $dom->createElement("city", utf8_encode($this->city));
        $city = $address->appendChild($city);

        $state = $dom->createElement("state", $this->state);
        $state = $address->appendChild($state);

        $country = $dom->createElement("country", $this->country);
        $country = $address->appendChild($country);

        $postalCode = $dom->createElement("postalCode", $this->postalCode);
        $postalCode = $address->appendChild($postalCode);

        return $address;
    }
}
