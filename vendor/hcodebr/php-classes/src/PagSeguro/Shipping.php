<?php
# *****
# File: Shipping.php
# Project: Curso de PHP com PagSeguro da Hcode
# Path: ~desenvolvedor/curso-de-php-com-pagseguro-hcode
# File Created: Wednesday, 06 July 2022 12:26:04
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Friday, 08 July 2022 18:04:47
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Classe Shipping.
# *****

namespace Hcode\PagSeguro;

use Hcode\Model\Address;
use DOMElement;
use DOMDocument;

class Shipping {
    const PAC = 1;
    const SEDEX = 2;
    const OTHER = 3;

    private $address;
    private $type;
    private $cost;
    private $addressRequired;

    public function __construct(
        Address $address,
        float $cost,
        int $type,
        bool $addressRequired = true // caso seja serviço, por exemplo, o objeto da venda não haverá endereço de entrega.
    ) {
        if ($type < 1 || $type > 3) {
            throw new \Exception("Informe um tipo de frete válido.");
        }

        $this->address = $address;
        $this->cost = $cost;
        $this->type = $type;
        $this->addressRequired = $addressRequired;
    }

    public function getDOMElement(): DOMElement {
        $dom = new DOMDocument();

        $shipping = $dom->createElement("shipping");
        $shipping = $dom->appendChild($shipping);

        $address = $this->address->getDOMElement();
        $address = $dom->importNode($address, true); // "true" para importar os nós filhos também
        $address = $shipping->appendChild($address);

        $cost = $dom->createElement("cost", number_format($this->cost, 2, ".", "")); // formatar o valor para o padrão do PagSeguro
        $cost = $shipping->appendChild($cost);

        $type = $dom->createElement("type", $this->type);
        $type = $shipping->appendChild($type);

        $addressRequired = $dom->createElement("addressRequired", ($this->addressRequired ? "true" : "false")); // "bool" no XML é string.
        $addressRequired = $shipping->appendChild($addressRequired);

        return $shipping;
    }
}
