<?php
# *****
# File: Payment.php
# Project: Curso de PHP com PagSeguro da Hcode
# Path: ~desenvolvedor/curso-de-php-com-pagseguro-hcode
# File Created: Wednesday, 06 July 2022 12:10:00
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Friday, 08 July 2022 19:15:40
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Classe Payment.
# *****

namespace Hcode\PagSeguro;

use DOMDocument;

class Payment {
    private $mode = "default";
    private $currency = "BRL";
    private $extraAmount = 0;
    private $reference = "";
    private $items = [];
    private $sender;
    private $shipping;
    private $method;
    private $creditCard;
    private $bank;

    public function __construct(
        string $reference,
        Sender $sender,
        Shipping $shipping,
        float $extraAmount = 0
    ) {
        $this->sender = $sender;
        $this->shipping = $shipping;
        $this->reference = $reference;
        $this->extraAmount = number_format($extraAmount, 2, ".", ""); // formatar o valor para o padrão do PagSeguro
    }

    public function getDOMDocument(): DOMDocument {
        $dom = new DOMDocument("1.0", "ISO-8859-1"); //LEMBRETE: (1) a versão do XML no PagSeguro é a "1.0", caso ocorra modificação modificar aqui. (2) caso o PagSeguro mude o encoding, alterar o ISO-8859-1 para o encoding correto.
        //UNDONE: parei aqui em 10m20s de 52m24s.
        return $dom;
    }
}
