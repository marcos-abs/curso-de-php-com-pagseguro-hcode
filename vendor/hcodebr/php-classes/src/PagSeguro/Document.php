<?php
# *****
# File: Document.php
# Project: Curso de PHP com PagSeguro da Hcode
# Path: ~desenvolvedor/curso-de-php-com-pagseguro-hcode
# File Created: Wednesday, 06 July 2022 12:19:57
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Thursday, 07 July 2022 11:31:16
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Classe Document.
# *****

namespace Hcode\PagSeguro;

use DOMElement;
use DOMDocument;

class Document {
    private $type;
    private $value;

    const CPF = "CPF";

    public function __construct(string $type, string $value) {
        if (!$value) {
            throw new \Exception("Informe o valor do documento.");
        }

        switch ($type) {
            case Document::CPF:
                if (!Document::isValidCPF($value)) {
                    throw new \Exception("O CPF informado não é válido.");
                }
                break;

            default:
                throw new \Exception("tipo não reconhecido.");
        }

        $this->type = $type;
        $this->value = $value;
    }

    public static function isValidCPF(string $number): bool {
        $number = preg_replace('/[^0-9]/', '', (string) $number);

        if (strlen($number) != 11)
            return false;

        for ($i = 0, $j = 10, $sum = 0; $i < 9; $i++, $j--)
            $sum += $number[$i] * $j;
        $rest = $sum % 11;
        if ($number[9] != ($rest < 2 ? 0 : 11 - $rest))
            return false;

        for ($i = 0, $j = 11, $sum = 0; $i < 10; $i++, $j--)
            $sum += $number[$i] * $j;
        $rest = $sum % 11;

        return ($number[10] == ($rest < 2 ? 0 : 11 - $rest));
    }

    public function getDOMElement(): DOMElement { // DOM = Document Object Model
        $dom = new DOMDocument();
        $document = $dom->createElement("document");
        $document = $dom->appendChild($document);
        $type = $dom->createElement("type", $this->type);
        $type = $document->appendChild($type);
        $value = $dom->createElement("value", $this->value);
        $value = $document->appendChild($value);

        return $document;
    }
}
