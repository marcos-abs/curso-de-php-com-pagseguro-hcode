<?php
# *****
# File: Transporter.php
# Project: Curso de PHP com PagSeguro da Hcode
# Path: ~desenvolvedor/curso-de-php-com-pagseguro-hcode
# File Created: Monday, 17 May 2021 13:20:25
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Monday, 04 July 2022 13:06:07
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2021 - 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Rotas do PagSeguro.
# *****

namespace Hcode\PagSeguro;

use \GuzzleHttp\Client;

class Transporter {

    public static function createSession() {
        $client = new Client();
        $resp = $client->request('POST', Config::getUrlSessions() . "?" . http_build_query(Config::getAuthentication()), [
            'verify' => false // para desativar a verificação do site seguro com SSL.
        ]);
        $xml = simplexml_load_string($resp->getBody()->getContents());
        // var_dump((string)$xml->id);
        return (string)$xml->id;
    }
}
