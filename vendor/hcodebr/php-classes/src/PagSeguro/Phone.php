<?php
# *****
# File: Phone.php
# Project: Curso de PHP com PagSeguro da Hcode
# Path: ~desenvolvedor/curso-de-php-com-pagseguro-hcode
# File Created: Wednesday, 06 July 2022 12:18:36
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Thursday, 07 July 2022 13:02:48
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Classe Phone.
# *****

namespace Hcode\PagSeguro;

use DOMElement;
use DOMDocument;



class Phone {
    private $areaCode;
    private $number;

    public function __construct(int $areaCode, int $number) {
        if (!$areaCode || $areaCode < 11 || $areaCode > 99) {
            throw new \Exception("Informe o DDD do telefone.");
        }
        if (!$number || strlen($number) < 8 || strlen($number) > 9) {
            throw new \Exception("Informe o número do telefone.");
        }
        $this->areaCode = $areaCode;
        $this->number = $number;
    }
    public function getDOMElement(): DOMElement {
        $dom = new DOMDocument();

        $phone = $dom->createElement("phone");
        $phone = $dom->appendChild($phone);

        $areaCode = $dom->createElement("areaCode", $this->areaCode);
        $areaCode = $phone->appendChild($areaCode);

        $number = $dom->createElement("number", $this->number);
        $number = $phone->appendChild($number);

        return $phone;
    }
}
