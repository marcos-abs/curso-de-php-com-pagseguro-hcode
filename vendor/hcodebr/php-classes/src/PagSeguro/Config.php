<?php
# *****
# File: Config.php
# Project: Curso de PHP com PagSeguro da Hcode
# Path: ~desenvolvedor/curso-de-php-com-pagseguro-hcode
# File Created: Monday, 17 May 2021 01:06:13
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Monday, 04 July 2022 19:45:03
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2021 - 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············Arquivo de configuração/integração com o PagSeguro. 
# *****

namespace Hcode\PagSeguro;

class Config {

    const SANDBOX = true;

    const SANDBOX_EMAIL = "desouza.marcos@uol.com.br";
    const PRODUCTION_EMAIL = "desouza.marcos@uol.com.br";

    const SANDBOX_TOKEN = "C8F11145308147158C680C6329B182D5";
    const PRODUCTION_TOKEN = "3E79D777D66A414D99C45E1ACDC54AE5";

    const SANDBOX_SESSIONS = "https://ws.sandbox.pagseguro.uol.com.br/v2/sessions";
    const PRODUCTION_SESSIONS = "https://ws.pagseguro.uol.com.br/v2/sessions";

    const SANDBOX_URL_JS = "https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js";
    const PRODUCTION_URL_JS = "https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js";

    const MAX_INSTALLMENT_NO_INTEREST = 10; // Máximo de parcelas sem juros
    const MAX_INSTALLMENT = 10; // Máximo de parcelas permitidas

    public static function getAuthentication(): array {
        if (Config::SANDBOX === true) {
            return [
                "email" => Config::SANDBOX_EMAIL,
                "token" => Config::SANDBOX_TOKEN
            ];
        } else {
            return [
                "email" => Config::PRODUCTION_EMAIL,
                "token" => Config::PRODUCTION_TOKEN
            ];
        }
    }

    public static function getUrlSessions(): string {
        return (Config::SANDBOX === true) ? Config::SANDBOX_SESSIONS : Config::PRODUCTION_SESSIONS; //gostei(!!)
    }

    public static function getUrlJS(): string {
        return (Config::SANDBOX === true) ? Config::SANDBOX_URL_JS : Config::PRODUCTION_URL_JS;
    }
}
