<?php
# *****
# File: CreditCard.php
# Project: Curso de PHP com PagSeguro da Hcode
# Path: ~desenvolvedor/curso-de-php-com-pagseguro-hcode
# File Created: Wednesday, 06 July 2022 12:24:00
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Friday, 08 July 2022 18:07:46
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Class CreditCard.
# *****

namespace Hcode\PagSeguro;

use Hcode\PagSeguro\CreditCard\Holder;
use Hcode\PagSeguro\CreditCard\Installment;

class CreditCard {
    private $token;
    private $installment;
    private $holder;
    private $billingAddress;

    public function __construct(
        string $token,
        Installment $installment,
        Holder $holder,
        Address $billingAddress
    ) {
        if (!$token) {
            throw new \Exception("Informe o token do cartão de crédito.");
        }

        $this->token = $token;
        $this->installment = $installment;
        $this->holder = $holder;
        $this->billingAddress = $billingAddress;
    }

    public function getDOMElement(): \DOMElement {
        $dom = new \DOMDocument();

        $creditCard = $dom->createElement("creditCard");
        $creditCard = $dom->appendChild($creditCard);

        $token = $dom->createElement("token", $this->token);
        $token = $creditCard->appendChild($token);

        $installment = $this->installment->getDOMElement();
        $installment = $dom->importNode($installment, true); // "true" para importar os nós filhos também
        $installment = $creditCard->appendChild($installment);

        $holder = $this->holder->getDOMElement();
        $holder = $dom->importNode($holder, true); // "true" para importar os nós filhos também
        $holder = $creditCard->appendChild($holder);

        $billingAddress = $this->billingAddress->getDOMElement("billingAddress");
        $billingAddress = $dom->importNode($billingAddress, true); // "true" para importar os nós filhos também
        $billingAddress = $creditCard->appendChild($billingAddress);

        return $creditCard;
    }
}
