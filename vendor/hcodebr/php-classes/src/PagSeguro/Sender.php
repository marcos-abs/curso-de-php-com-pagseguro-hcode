<?php
# *****
# File: Sender.php
# Project: Curso de PHP com PagSeguro da Hcode
# Path: ~desenvolvedor/curso-de-php-com-pagseguro-hcode
# File Created: Wednesday, 06 July 2022 12:15:50
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Thursday, 07 July 2022 20:03:18
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Clase Sender.
# *****

namespace Hcode\PagSeguro;

use DateTime;
use DOMElement;
use DOMDocument;

class Sender {
    private $name;
    private $cpf;
    private $bornDate;
    private $phone;
    private $email;
    private $hash;
    private $ip;

    public function __construct(
        string $name,
        Document $cpf,
        DateTime $bornDate,
        Phone $phone,
        string $email,
        string $hash,
        string $ip
    ) {
        if (!$name) {
            throw new \Exception("Informe o nome do comprador.");
        }
        if (!$cpf) {
            throw new \Exception("Informe o CPF do comprador.");
        }
        if (!$bornDate) {
            throw new \Exception("Informe a data de nascimento do comprador.");
        }
        if (!$phone) {
            throw new \Exception("Informe o número do telefone do comprador.");
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) { // legal esse filtro (!!)
            throw new \Exception("Informe o endereço de e-mail do comprador.");
        }
        if (!$hash) {
            throw new \Exception("Informe a identificação do comprador.");
        }
        $this->$name = $name;
        $this->$cpf = $cpf;
        $this->$bornDate = $bornDate;
        $this->$phone = $phone;
        $this->$email = $email;
        $this->$hash = $hash;
        $this->$ip = $_SERVER['REMOTE_ADDR'];
    }
    public function getDOMElement(): DOMElement {
        $dom = new DOMDocument();

        $sender = $dom->createElement("sender");
        $sender = $dom->appendChild($sender);

        $name = $dom->createElement("name", $this->name);
        $name = $sender->appendChild($name);

        $email = $dom->createElement("email", $this->email);
        $email = $sender->appendChild($email);

        $bornDate = $dom->createElement("bornDate", $this->bornDate->format("d/m/Y")); // formatar a data para padrão do PagSeguro
        $bornDate = $sender->appendChild($bornDate);

        $documents = $dom->createElement("documents");
        $documents = $sender->appendChild($documents);

        $cpf = $this->cpf->getDOMElement();
        $cpf = $dom->importNode($cpf, true); // "true" para importar os nós filhos também
        $cpf = $documents->appendChild($cpf);

        $phone = $this->phone->getDOMElement();
        $phone = $dom->importNode($phone, true); // "true" para importar os nós filhos também
        $phone = $documents->appendChild($phone);

        $hash = $dom->createElement("hash", $this->hash);
        $hash = $sender->appendChild($hash);

        $ip = $dom->createElement("ip", $this->ip);
        $ip = $sender->appendChild($ip);

        return $sender;
    }
}
