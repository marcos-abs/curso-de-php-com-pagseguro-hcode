<?php
# *****
# File: Installment.php
# Project: Curso de PHP com PagSeguro da Hcode
# Path: ~desenvolvedor/curso-de-php-com-pagseguro-hcode
# File Created: Wednesday, 06 July 2022 12:39:36
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Friday, 08 July 2022 17:39:34
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Classe Installment (prestações ou parcelas).
# *****

namespace Hcode\PagSeguro\CreditCard;

use Hcode\PagSeguro\Config;

class Installment {
    private $quantity;
    private $value;

    public function __construct(
        int $quantity, //LEMBRETE: aqui é relativo a quantidade de parcelas e não de itens.
        float $value
    ) {
        if ($quantity < 1 || $quantity > Config::MAX_INSTALLMENT) {
            throw new \Exception("Informe uma quantidade de parcelas válida.");
        }
        if ($value <= 0) { //LEMBRETE: aqui é relativo ao valor total e não ao valor da parcela.
            throw new \Exception("Informe um valor total da compra válido.");
        }
        $this->quantity = $quantity;
        $this->value = $value;
    }
    public function getDOMElement(): \DOMElement {
        $dom = new \DOMDocument();

        $installment = $dom->createElement("installment");
        $installment = $dom->appendChild($installment);

        $value = $dom->createElement("value", number_format($this->value, 2, ".", "")); // formatar o valor para o padrão do PagSeguro
        $value = $installment->appendChild($value);

        $quantity = $dom->createElement("quantity", $this->quantity);
        $quantity = $installment->appendChild($quantity);

        $noInterestInstallmentQuantity = $dom->createElement("noInterestInstallmentQuantity", Config::MAX_INSTALLMENT_NO_INTEREST);
        $noInterestInstallmentQuantity = $installment->appendChild($noInterestInstallmentQuantity);


        return $installment;
    }
}
