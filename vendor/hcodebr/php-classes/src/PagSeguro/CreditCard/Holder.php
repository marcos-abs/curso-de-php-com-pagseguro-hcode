<?php
# *****
# File: Holder.php
# Project: Curso de PHP com PagSeguro da Hcode
# Path: ~desenvolvedor/curso-de-php-com-pagseguro-hcode
# File Created: Wednesday, 06 July 2022 12:37:14
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Thursday, 07 July 2022 20:23:51
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Classe Holder (quem paga - comprador).
# *****

namespace Hcode\PagSeguro\CreditCard;

use DateTime;
use Hcode\PagSeguro\Document;
use Hcode\PagSeguro\Phone;
use DOMElement;
use DOMDocument;

class Holder {
    private $name;
    private $cpf;
    private $birthDate;
    private $phone;

    public function __construct(
        string $name,
        Document $cpf,
        DateTime $birthDate,
        Phone $phone
    ) {
        if (!$name) {
            throw new \Exception("Informe o nome do comprador.");
        }

        $this->name = $name;
        $this->cpf = $cpf;
        $this->birthDate = $birthDate;
        $this->phone = $phone;
    }

    public function getDOMElement(): DOMElement {
        $dom = new DOMDocument();

        $holder = $dom->createElement("holder");
        $holder = $dom->appendChild($holder);

        $name = $dom->createElement("name", $this->name);
        $name = $holder->appendChild($name);

        $birthDate = $dom->createElement("birthDate", $this->birthDate->format("d/m/Y")); // formatar a data para padrão do PagSeguro
        $birthDate = $holder->appendChild($birthDate);

        $documents = $dom->createElement("documents");
        $documents = $holder->appendChild($documents);

        $cpf = $this->cpf->getDOMElement();
        $cpf = $dom->importNode($cpf, true); // "true" para importar os nós filhos também
        $cpf = $documents->appendChild($cpf);

        $phone = $this->phone->getDOMElement();
        $phone = $dom->importNode($phone, true); // "true" para importar os nós filhos também
        $phone = $documents->appendChild($phone);

        return $holder;
    }
}
