<?php
# *****
# File: Item.php
# Project: Curso de PHP com PagSeguro da Hcode
# Path: ~desenvolvedor/curso-de-php-com-pagseguro-hcode
# File Created: Wednesday, 06 July 2022 12:21:48
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Friday, 08 July 2022 18:46:26
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Classe Item.
# *****

namespace Hcode\PagSeguro;

class Item {
    private $id;
    private $description;
    private $amount;
    private $quantity;

    public function __construct(
        int $id,
        string $description,
        float $amount,
        int $quantity //LEMBRETE: caso a quantidade de itens seja em decimais, utilize o float.
    ) {
        if (!$id || !$id > 0) {
            throw new \Exception("Informe o id do item.");
        }
        if (!$description || strlen($description) < 3) {
            throw new \Exception("Informe a descrição do item.");
        }
        if (!$amount || !$amount > 0) {
            throw new \Exception("Informe o valor total do item.");
        }
        if (!$quantity || !$quantity > 0) {
            throw new \Exception("Informe a quantidade do item.");
        }
        $this->id = $id;
        $this->description = $description;
        $this->amount = $amount;
        $this->quantity = $quantity;
    }
    public function getDOMElement(): \DOMElement {
        $dom = new \DOMDocument();

        $item = $dom->createElement("item");
        $item = $dom->appendChild($item);

        $amount = $dom->createElement("amount", number_format($this->amount, 2, ".", "")); // formatar o valor para o padrão do PagSeguro
        $amount = $item->appendChild($amount);

        $description = $dom->createElement("description", $this->description);
        $description = $item->appendChild($description);

        $quantity = $dom->createElement("quantity", $this->quantity);
        $quantity = $item->appendChild($quantity);

        return $item;
    }
}
